package com.ecommerce.glidegif

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Glide.with(this).asGif().load(R.drawable.test1).apply(RequestOptions().override(100, 100)).into(imageView1)
        Glide.with(this).asGif().load(R.drawable.test2).apply(RequestOptions().override(100, 100)).into(imageView2)
        Glide.with(this).asGif().load(R.drawable.test3).apply(RequestOptions().override(100, 100)).into(imageView3)
        Glide.with(this).asGif().load(R.drawable.test4).apply(RequestOptions().override(100, 100)).into(imageView4)
        Glide.with(this).asGif().load(R.drawable.test5).apply(RequestOptions().override(100, 100)).into(imageView5)
        Glide.with(this).asGif().load(R.drawable.test6).apply(RequestOptions().override(100, 100)).into(imageView6)
    }
}
